const logoPorto = document.querySelector('img[alt="Porto"]');
const sidebarColLg3 = document.querySelector(".container > .row > .col-lg-3");
const sidebarColLg9 = document.querySelector(".container > .row > .col-lg-9");
const formAddToCart = document.querySelector("form.cart");
const parentNode = formAddToCart.parentNode; // Parent du formulaire d'ajout au panier
// const parentNode = document.querySelector('.summary.entry-summary');
const categories = document.querySelector(".product-meta");
const text = document.querySelector(".summary.entry-summary > :nth-child(4)");
const counter = document.querySelector(".quantity.quantity-lg");
const formButton = document.querySelector("form.cart > button.btn");
const formParent = document.querySelector(".summary.entry-summary");
const subscribeButton = document.querySelector("#newsletterForm > div > span > button");
const inputSubscribe = document.querySelector("#newsletterForm > div.input-group");
const allImgCarousel = document.querySelectorAll("div.owl-carousel > div > img");
const allRelatedProductsImg = document.querySelectorAll("div.masonry-loader img");
console.log(allRelatedProductsImg);

/**
 * Applique d'autres images aux carousel
 */
const changeCarouselImg = () => {
    allImgCarousel.forEach((oneImageCarousel, index) => {
        oneImageCarousel.setAttribute('src', `img/products/sac${index + 1}.jpg`);
    });
};

/**
 * Applique d'autres images à la section Related Products
 */
const changeRelatedProductsImage = () => {
    allRelatedProductsImg.forEach((oneImage, index) => {
        oneImage.setAttribute('src', `img/products/image${index + 1}.PNG`);
    })
}

/**
 * Modification du logo Porto (noir => bleu)
 */
const changeLogo = () => {
    logoPorto.setAttribute("src", "img/logo-default.png");
};

/**
 * Suppression de la sidebar
 */
const removeSideBar = () => {
    // Display: none sur la partie droite de main
    // sidebarColLg3.style.display = "none";
    sidebarColLg3.remove();
};

/**
 * Remplace col-lg-9 par col-lg-12
 */
const col9ByCol12 = () => {
    // Passage de 9 colonnes à 12 colonnes pour occuper tout l'espace
    if (sidebarColLg9.classList.contains("col-lg-9")) {
        sidebarColLg9.classList.replace("col-lg-9", "col-lg-12");
    }
};

/**
 * Echanger formulaire d'ajout au panier avec Categories
 * Inutile quand on passe le formulaire en position absolute
 */
const invertFormAndCategories = () => {
    // Passer le formulaire d'ajout au panier après les Categories
    parentNode.insertBefore(categories, formAddToCart);
    parentNode.appendChild(formAddToCart);
};

/**
 * Ajuste l'espace entre la le texte et Categories
 */
const spaceBetweenTextAndCategories = () => {
    // Modification espace entre texte et Categories
    if (text.classList.contains("mb-5")) {
        text.className = "mb-4";
    }
};

const formStyle = () => {
    // Passer le bouton du formulaire en dessous du compteur
    counter.style.float = "none";
};

/**
 * Modifier le bouton du formulaire d'ajout au panier
 */
const styleButtonAddToCart = () => {
    formButton.classList.add("btn-lg");
    formButton.style.padding = "20px 30px";
    formButton.style.display = "block";
    formButton.style.margin = "25px auto";
};

/**
 * Centrer l'input compteur
 */
const centerInputCounter = () => {
    counter.style.margin = "25px auto";
};

/**
 * Positionner le formulaire en bas de son parent
 */
const putFormBottom = () => {
    formParent.style.position = "relative";
    formParent.style.height = "100%";
    formAddToCart.style.position = "absolute";
    formAddToCart.style.bottom = "25px";
    formAddToCart.style.left = "50%";
    formAddToCart.style.transform = "translateX(-50%)";
};

/**
 * Bouton Subcribe dans footer sans border radius
 */
const modifySubscribButton = () => {
    subscribeButton.style.borderTopRightRadius = "0";
    subscribeButton.style.borderBottomRightRadius = "0";
};

const addIconSubscribe = () => {
    const img = document.createElement("img");
    img.setAttribute("src", "img/icons/input-icon.png");
    inputSubscribe.style.position = "relative";
    img.style.position = "absolute";
    img.style.top = "50%";
    img.style.transform = "translateY(-50%)";
    img.style.right = "33%";
    img.style.zIndex = "999";
    img.style.width = "17px";
    img.style.cursor = "pointer";
    inputSubscribe.appendChild(img);
};

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function deleteCookie(name) {
    document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/;`;
}

const switchButton = document.createElement("button");
switchButton.setAttribute("id", "switchButton");
if (getCookie("version") === "A") {
    switchButton.innerText = "Afficher version B";
} else if (getCookie("version") === "B" || getCookie("version") === null) {
    switchButton.innerText = "Afficher version A";
}

switchButton.style.position = "fixed";
switchButton.style.top = "10px";
switchButton.style.left = "10px";
switchButton.style.zIndex = "999999";
document.body.appendChild(switchButton);

const initTheme = () => {
    let version = getCookie("version");
    if (version === "B") {
        changeLogo();
        changeCarouselImg();
        removeSideBar();
        col9ByCol12();
        invertFormAndCategories();
        spaceBetweenTextAndCategories();
        formStyle();
        styleButtonAddToCart();
        centerInputCounter();
        putFormBottom();
        changeRelatedProductsImage();
        modifySubscribButton();
        addIconSubscribe();
    }
};

switchButton.addEventListener("click", () => {
    if (getCookie("version") === "A") {
        switchButton.innerText = "Afficher version B";
        setCookie("version", "B", 30);
        document.location.reload();
    } else if (getCookie("version") === "B") {
        switchButton.innerText = "Afficher version A";
        setCookie("version", "A", 30);
        document.location.reload();
    }
});

initTheme();
